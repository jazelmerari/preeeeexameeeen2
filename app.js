document.getElementById("cargarRazasButton").addEventListener("click", function() {
    var select = document.getElementById("breedSelect");
    select.innerHTML = "";

    // Llamada a la API para obtener la lista de razas
    fetch("https://dog.ceo/api/breeds/list")
        .then(response => response.json())
        .then(data => {
            data.message.forEach(breed => {
                var option = document.createElement("option");
                option.value = breed;
                option.text = breed;
                select.appendChild(option);
            });
        })
        .catch(error => console.error("Error al cargar las razas:", error));
});

document.getElementById("verImagenButton").addEventListener("click", function() {
    var selectedBreed = document.getElementById("breedSelect").value;
    var dogImage = document.getElementById("dogImage");

    fetch(`https://dog.ceo/api/breed/${selectedBreed}/images/random`)
        .then(response => response.json())
        .then(data => {
            dogImage.src = data.message;
        })
        .catch(error => console.error("Error al cargar la imagen:", error));
});